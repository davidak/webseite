<!--
.. title: Linux Foto Workflow
.. slug: linux-foto-workflow
.. date: 2022-02-22 10:46:59 UTC+01:00
.. tags: Foto, Linux
.. category:
.. link:
.. description:
.. type: text
-->

Hier habe ich meinen Workflow dokumentiert, um Fotos auf Linux zu bearbeiten.

## 0. Bildschirm kalibrieren

Um sinnvoll Fotos bearbeiten zu können sollte man den Bildschirm kalibrieren.

Da Bildschirme mit der Zeit schlechter werden sollte man das einmal im Monat wiederholen.

Ich verwende das Programm [DisplayCAL](https://displaycal.net/) und ein [Calibrite ColorChecker Display Pro](https://calibrite.com/de/product/colorchecker-display-pro/) Kolorimeter (früher von X-Rite).

## 1. Fotos machen

Die Fotos werden in **RAW** gespeichert, um bei der Bearbeitung möglichst viel Flexibilität zu haben.

Ich experimentiere aber auch mit der Filmsimulation meiner [Fujifilm Kameras](https://fujifilm-x.com/de-de/products/cameras/), wodurch das Bearbeiten eventuell wegfällt. Hier wird direkt als **JPEG** gespeichert.

## 2. Fotos übertragen

Ich nutze [Rapid Photo Downloader](https://damonlynch.net/rapid/), um die Fotos und Videos auf mein [NAS](https://de.wikipedia.org/wiki/Network_Attached_Storage) zu übertragen und in entsprechende Ordner einzusortieren.

Die Ordner haben die Struktur: `Fotos/YYYY/YYYY-MM-DD`.

Wobei `YYYY` für das Jahr steht, z.B. 2022,  
`MM` für den Monat, z.B. 02,  
und `DD` für den Tag, z.B. 22.

## 3. Fotos bearbeiten

Die Fotos vom NAS importiere ich dann in [darktable](https://www.darktable.org/) und bearbeite sie.

Um die Bedienung des Programms zu lernen benutze ich den [Open Source Photography Course](https://lessons.rileybrandt.com/courses/fossphotocourse) von [Riley Brandt](https://www.rileybrandt.com/).

## 4. Fotos veröffentlichen

Bisher habe ich [Flickr](https://flickr.com/photos/davidak/) benutzt, bin aber nach dem Verkauf der Plattform nicht mehr zufrieden.

Ich suche eine unabhängige Platform, die von der Community betrieben wird. [Pixelfed](https://pixelfed.org/) werde ich mir mal genauer ansehen.
