.. title: Empfehlungen
.. slug: empfehlungen
.. date: 2018-03-08 18:56:40 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text

Ich treffe informierte Entscheidungen, denen oft eine längere Recherche zugrunde liegt. Damit auch Andere davon profitieren habe ich diese Seite erstellt.

Meine Kriterien sind dabei:

- es muss das Problem lösen
- es muss zuverlässig funktionieren
- es sollte für alle verfügbar sein (z.B. unter freier Lizenz oder zu fairem Preis)
- es sollte für alle benutzbar sein (funktionales Design und intuitive Bedienung)
- es sollte nachhaltig hergestellt werden

Teilweise müssen Kompromisse gemacht werden, wenn keine der vorhandenen Lösungen dem Anspruch gerecht wird.

**Übersicht**

[TOC]

# Software

## Betriebssysteme

### [elementary OS](https://elementary.io/)

Elegante und benutzerfreundliche GNU/Linux-Distribution. Gefällt mir als ehemaliger Mac-User sehr gut!

Baut auf Ubuntu LTS auf, wodurch man Zugriff auf über 25.000 Software-Pakete hat.

Für Linux-Einsteiger geeignet.

### [NixOS](https://nixos.org/)

Innovative GNU/Linux-Distribution, bei der das System durch eine Konfigurationsdatei bestimmt wird. Besonders praktisch für Server, da **Configuration Management** ins Betriebssystem integriert ist, aber auch für erfahrene GNU/Linux-Nutzer, die die Flexibilität von Arch Linux oder Gentoo mögen, aber auf kompilierte Pakete und stabile Versionen zurückgreifen möchten.

Es gibt alle 6 Monate einen stabilen Release und einen unstable rolling Release Channel.

Es hat mehr Pakete als jede andere Distribution (fast 70.000) und die meisten Pakete in der neusten Version. Siehe Statistik auf [repology.org](https://repology.org/).

Meine Konfigurationen findest du auf [Codeberg](https://codeberg.org/davidak/nixos-config).

### [LineageOS for microG](https://lineage.microg.org/)

[LineageOS](https://lineageos.org/) ist eine Freie Android-Distribution. Diese Variante hat [microG](https://microg.org/) und [F-Droid](https://f-droid.org/de/) bereits vorinstalliert.

Es gibt regelmäßig Updates, auch wenn der Hersteller das Gerät nicht mehr unterstützt.

Die Installation eines Betriebssystems auf einem Android-Smartphone ist recht kompliziert. Wenn du damit keine Erfahrung hast such dir Unterstützung. Anlaufstellen wären z.B. [Repair Cafes](https://repaircafe.org/de/) oder [Hackerspaces](https://hackerspaces.org/).

## Android

### App-Stores

- [F-Droid](https://f-droid.org/de/) - Android App-Store mit ausschließlich Freier Software
- [Aurora Store](https://f-droid.org/de/packages/com.aurora.store/) - Apps aus dem Google Play Store installieren und aktualisieren

### Apps

- [K-9 Mail](https://f-droid.org/de/packages/com.fsck.k9/) - E-Mail-Client
- [Element](https://f-droid.org/de/packages/im.vector.app/) - Client für das dezentrale Matrix Messanger-Protokoll
- [Signal](https://signal.org/android/apk/) - Sicherer und benutzerfreundlicher Messanger, der allerdings einen zentralen Server nutzt
- [Mumla](https://f-droid.org/de/packages/se.lublin.mumla/) - Mumble Client
- [KeePass DX](https://f-droid.org/de/packages/com.kunzisoft.keepass.libre/) - Passwortmanager (kompatibel mit [KeePass](https://keepass.info/) und [KeePassXC](https://keepassxc.org/))
- [andOTP](https://f-droid.org/de/packages/org.shadowice.flocke.andotp/) - Zwei-Faktor-Authentifizierung
- [Corona Contact Tracing Germany](https://f-droid.org/de/packages/de.corona.tracing/) - Corona-Kontaktverfolgung, Impfzertifikat Speicher und Kontakt-Tagebuch (Komplett Freier Fork der CWA)
- [Sleep as Android](https://sleep.urbandroid.org/) - Schlaftracking (Bezahlte App aus dem Play Store. Keine Freie Software!)
- [Schrittzähler](https://f-droid.org/de/packages/org.secuso.privacyfriendlyactivitytracker/) - Schritte im Hintergrund zählen
- [Meditation Assistant](https://f-droid.org/de/packages/sh.ftp.rocketninelabs.meditationassistant.opensource/) - Meditationstimer und Statistik
- [Loop Habit Tracker](https://f-droid.org/de/packages/org.isoron.uhabits/) - Gewohnheiten und langfristige Ziele verfolgen
- [Syncthing](https://f-droid.org/de/packages/com.nutomic.syncthingandroid/) - Dateien mit anderen Geräten synchronisieren über ein P2P-Netzwerk (ohne Cloud!)
- [OAndBackupX](https://f-droid.org/de/packages/com.machiav3lli.backup/) - Backup von Apps und Daten
- [Audio Recorder](https://f-droid.org/en/packages/com.github.axet.audiorecorder/) - Audio aufzeichnen in verschiedenen Formaten
- [ScreenCam](https://f-droid.org/de/packages/com.orpheusdroid.screenrecorder/) - Video vom Bildschirm aufzeichnen
- [Barcode-Scanner](https://f-droid.org/de/packages/com.google.zxing.client.android/) - Einfacher Scanner für viele Arten von Barcodes, QR-Codes etc.
- [OsmAnd](https://f-droid.org/de/packages/net.osmand.plus/) - Navigation mit der [OpenStreetMap](https://www.openstreetmap.org/) (auch offline!)
- [StreetComplete](https://f-droid.org/de/packages/de.westnordost.streetcomplete/) - Zu [OpenStreetMap](https://www.openstreetmap.org/) beitragen durch Lösen von einfachen Aufgaben, z.B. Hausnummer eintragen
- [Vespucci](https://f-droid.org/de/packages/de.blau.android/) - Erweiterter [OpenStreetMap](https://www.openstreetmap.org/)-Editor
- [Transportr](https://f-droid.org/de/packages/de.grobox.liberario/) - Verbindungen mit öffentlichem Personennahverkehr (ÖPNV) finden
- [Wetter](https://f-droid.org/de/packages/org.secuso.privacyfriendlyweather/) - Wettervorhersage für den aktuellen Tag und Rest der Woche
- [Suntimes](https://f-droid.org/de/packages/com.forrestguice.suntimeswidget) - Uhrzeit des Sonnenaufgangs und Sonnenuntergangs des aktuellen Tages anzeigen
- [PassAndroid](https://f-droid.org/de/packages/org.ligi.passandroid/) - App für Tickets als Passbook-Dateien oder QR-Code
- [Music Player GO](https://f-droid.org/de/packages/com.iven.musicplayergo/) - Einfaches Musik-Abspielprogramm
- [NewPipe](https://f-droid.org/de/packages/org.schabi.newpipe/) - Videos von [YouTube](https://www.youtube.com/) schauen
- [Shattered Pixel Dungeon](https://f-droid.org/de/packages/com.shatteredpixel.shatteredpixeldungeon/) Klassisches [Roguelike](https://de.wikipedia.org/wiki/Rogue-like) RPG mit Grafik im Pixel-Art Stil

## [Chromium](https://www.chromium.org/Home) (Webbrowser)

Performanter, stabiler und sicherer Open Source-Browser. Basis von Chrome, aber [ohne das Tracking](https://de.wikipedia.org/wiki/Chromium_(Browser)#Unterschiede_zu_Google_Chrome).

## Chrome-/Chromium-Extensions (Browser-Erweiterungen)

- [uBlock Origin](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm) - Effizienter Blocker für Tracker, Malware und Werbung. Mehr Performance und Sicherheit!
- [Axel Springer Blocker](https://chrome.google.com/webstore/detail/axel-springer-blocker-asb/cbnipbdpgcncaghphljjicfgmkonflee) - Blockiert die Propaganda des Axel Springer-Konzerns
- [SponsorBlock for YouTube](https://chrome.google.com/webstore/detail/sponsorblock-for-youtube/mnjggcdmjocbbbhaepdhchncahnbgone) - Sponsor-Segmente in YouTube-Videos überspringen
- [Return YouTube Dislike](https://chrome.google.com/webstore/detail/return-youtube-dislike/gebbhagfogifgggkldgodflihgfeippi) - Bringt Dislike-Feature auf YouTube zurück. Nutzt Bewertungen von Benutzern der Extension
- [YouTube Captions Search](https://chrome.google.com/webstore/detail/youtube-captions-search/kimbeggjgnmckoikpckibeoaocafcpbg) - Untertitel auf YouTube durchsuchen
- [HTTPS Everywhere](https://chrome.google.com/webstore/detail/https-everywhere/gcbommkclmclpchllfjekcdonpmejbdp) - HTTP auf HTTPS weiterleiten (für bekannte Domains)
- [Terms of Service; Didn’t Read](https://chrome.google.com/webstore/detail/hjdoplcnndgiblooccencgcggcoihigg) - Bewertung von Nutzungsbedingungen im Hinblick auf Privatsphäre
- [The Great Suspender](https://chrome.google.com/webstore/detail/the-great-suspender/klbibkeccnjlkjkiokjodocebajanakg) - Ungenutzte Tabs nach einiger Zeit sperren, um Arbeitsspeicher und CPU frei zu geben
- [Auto Refresh](https://chrome.google.com/webstore/detail/auto-refresh/ifooldnmmcmlbdennkpdnlnbgbmfalko) - Webseite nach einstellbarem Intervall neu laden
- [Page load time](https://chrome.google.com/webstore/detail/page-load-time/fploionmjgeclbkemipmkogoaohcdbig) - Seitenladezeit anzeigen und aufschlüsseln
- [Tab Counter](https://chrome.google.com/webstore/detail/tab-counter/feeoiklfggbaibpdhkkngbpkppdmcjal) - Anzahl der offenen Tabs und Fenster anzeigen
- [ModHeader](https://chrome.google.com/webstore/detail/modheader/idgpnmonknjnojddfkpgkljpfnnfcklj) - Benutzerdefinierte Header setzen
- [KeePassXC-Browser](https://chrome.google.com/webstore/detail/keepassxc-browser/oboonakemofpalcgghocfoadofidjkkk) - KeePassX-Integration für den Browser
- [IPFS Station](https://chrome.google.com/webstore/detail/ipfs-station/kckhgoigikkadogfdiojcblegfhdnjei) - [IPFS](https://ipfs.io/)-Ressourcen über lokale Node laden

## Spiele

- [MultiMC](https://multimc.org/) Minecraft-Client Instanzen verwalten
- [Mumble](https://mumble.info/) Voice-Chat mit geringer Latenz

# Hardware

## Computer

Ich habe gute Erfahrung mit gebrauchter Business-Hardware gemacht. Bei [ITSCO](https://www.itsco.de/) gibt es zum Beispiel [Desktop-PCs von HP](https://www.itsco.de/computer/hp-compaq-pcs), [Workstations von Dell](https://www.itsco.de/workstation/dell-workstation) oder [Notebooks von Lenovo](https://www.itsco.de/notebooks/lenovo-laptops). Letztere findest du auch bei [Thinkstore24.de](https://www.thinkstore24.de/laptops/?p=1&o=1&n=12&s=8).

<details>
<summary>mehr anzeigen</summary>
<p>
**Pro**

- sehr günstiger Preis im Vergleich zu neuer Hardware
- zuverlässig und langlebig im Gegensatz zu Consumer-Hardware

**Contra**

- nicht die aktuellste Technik
- eventuell Gebrauchsspuren
- eventuell fehlen Komponenten wie Festplatte. steht aber dabei und kann problemlos separat gekauft werden
- keine Garantie
</p>
</details>

## Festplatten (HDD)

Nachdem mir in der Vergangenheit Festplatten von Western Digital und Seagate nach weniger als 2 Jahren kaputt gegangen sind, verwende ich in meinem NAS nur noch [HGST Deskstar NAS](https://www.hgst.com/products/hard-drives/nas-desktop-drive-kit).

Diese Festplatten sind laut [Statistiken des Cloud Backup-Anbieters Backblaze](https://www.backblaze.com/b2/hard-drive-test-data.html) die Zuverlässigsten.

Allerdings sind die anderen Modelle in der Statistik auch ziemlich gut. Wichtig ist hier, für ein NAS, dass 24/7 läuft, auch spezielle NAS Festplatten zu verwenden.

Es lohnt sich nicht gebrauchte Festplatten zu kaufen.

## Solid-State-Drives (SSD)

Für die Systempartition eines Computers, auf der das Betriebssystem und Programme installiert sind, empfehle ich eine SSD. Diese haben eine spürbar höhere Lese- und Schreibgeschwindigkeit, wodurch Programme schneller starten und sich das Arbeiten flüssiger anfühlt. Sie besitzen keine mechanischen Teile, haben einen sehr geringen Energieverbrauch und arbeiten geräuschlos.

Ich habe folgende Modelle im Einsatz:

- Crucial M4 SSD
- SanDisk SSD PLUS
- SanDisk Ultra 3D SSD
- Samsung SSD 850 EVO

Diese funktionieren seit mehreren Jahren zuverlässig.

Da der Preis von SSDs stark fällt (bei manchen Modellen in einem Jahr halbiert!) und sich die Technik schnell weiterentwickelt, lohnt es sich nicht gebrauchte Hardware oder sehr alte Modelle zu kaufen.

# Infrastruktur

## E-Mail

### [mailbox.org](https://mailbox.org/)

### [posteo.de](https://posteo.de/)

# Lizenzen

## GNU General Public License (GPL)

Für Software Quelltext

Vergleich mit anderen Open Source-Lizenzen: <https://choosealicense.com/>

Deutsche Übersetzung: <http://www.gnu.de/documents/gpl.de.html>

## Creative Commons Attribution Share Alike (CC-BY-SA)

Für Medien und Daten wie Texte, Bilder, Videos und Audio

Webseite: <https://creativecommons.org/licenses/by-sa/4.0/>

# Lebensmittel

Möglichst unverarbeitet, Rohkost Anteil, regional und bio.

# Ernährung

Die gesündeste, nachhaltigste und ethisch korrekteste Ernährung ist rein pflanzlich (vegan).

Fakten zu dem Thema werden in diesem kurzen Video gut zusammengefasst:

{{% media url="https://youtu.be/8rK-USZ51TQ" %}}

**Achtung**: Vitamin B12 muss suplimentiert werden!

Informationen und gute Produkte bei diesem Händler: <https://www.sunday.de/vitamin-b12/>
