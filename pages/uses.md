.. title: Meine Computer
.. slug: uses
.. date: 2020-09-24 07:39:00 UTC+02:00
.. tags: computer, hardware, software
.. category:
.. link:
.. description:
.. type: text

All meine Arbeit findet zumindest teilweise am Computer statt. Hier siehst du, welche Hardware und Software ich verwende.

# Hardware

Aktuell verwende ich folgende Geräte.

## Lenovo Thinkpad X230

Seit Ende 2017 ist es mein Computer für unterwegs.

- Intel [i5-3320M](https://ark.intel.com/content/www/us/en/ark/products/64896/intel-core-i5-3320m-processor-3m-cache-up-to-3-30-ghz.html) Prozessor mit 2,6GHz und integrierter Grafikkarte
- 8 GB RAM
- 250 GB SSD
- 13" Bildschirm

## Workstation

Ende 2019 gekauft für [VR](https://de.wikipedia.org/wiki/Virtuelle_Realit%C3%A4t), Spiele, Videoschnitt und Software-Entwicklung.

- Intel [i9-9900K](https://ark.intel.com/content/www/us/en/ark/products/186605/intel-core-i9-9900k-processor-16m-cache-up-to-5-00-ghz.html) Prozessor mit 3,6GHz
- G.Skill Ripjaws 32 GB DDR4 RAM (3200 MHz)
- Samsung 970 EVO Plus 1TB NVMe M.2 SSD
- AMD Radeon RX 6600 XT Grafikkarte mit 8 GB VRAM
- Gigabyte Z390 UD Mainboard
- Noctua NH-D15 Kühler
- be quiet! Straight Power 11 650 Watt Netzteil    
- Antec One Midi Tower Gehäuse

## Bildschirm

Für mich funktioniert es gut einen großen Bildschirm zu haben statt mehrere Kleine.

Bei meiner Arbeit in der IT-Abteilung haben sich Dell Bildschirme als langlebig und qualitativ hochwertig herausgestellt.

[Dell UP2716D 27" 2.5K IPS LED Display](https://www.dell.com/de-de/shop/accessories/apd/210-AGTR)

Gebraucht hab ich diesen etwa zum halben Preis bekommen.

## VR-Headset

Das [Valve Index](https://store.steampowered.com/valveindex) HMD unterstützt offiziell Linux.

## Tastatur

Nach langem Überlegen habe ich mich dazu entschieden, eine **mechanische, ergonomische Tastatur** zu kaufen.

[Ergodox EZ](http://ergodox-ez.com/)

Damit verwende ich das ergonomische [BONE Tastatur-Layout](https://neo-layout.org/Layouts/bone/) (Eine Weiterentwicklung von NEO2).

Ich bin damit sehr zufrieden.

## Maus

Ich verwende die **ergonogische** [G403 HERO Gaming-Maus](https://www.logitechg.com/de-ch/products/gaming-mice/g403-hero-gaming-mouse.910-005632.html). Guter Kompromiss zwischen Ergonomie und Performance zu einem fairen Preis.

Ich habe auch eine Zeit lang eine **vertikale** Maus verwendet, was im Prinzip sehr **ergonomisch** ist, aber für Gaming nicht funktioniert.

## Kopfhörer

Ich habe einen [beyerdynamic DT 770 Pro](https://www.beyerdynamic.de/dt-770-pro.html) (geschlossere Bauweise) und einen [beyerdynamic DT 990 Pro](https://www.beyerdynamic.de/dt-990-pro.html) (offene Bauweise) mit einem [ModMic Uni](https://antlionaudio.com/products/modmic-uni) als Headset.

Für eine bessere Audiowiedergabe schließe ich den Kopfhörer an ein [Sharkoon Gaming DAC Pro S](https://en.sharkoon.com/product/27415) an.

Zudem habe ich den [Sony WH-1000XM3](https://www.sony.de/electronics/kopfband-kopfhoerer/wh-1000xm3) Bluetooth-Kopfhörer mit Noise Cancelling.

### Was ist der Unterschied zwischen einem offenen und einem geschlossenen Kopfhörer?

Ein Kopfhörer mit offener Bauweise hat einen räumlicheren Sound, aber der Ton ist für alle hörbar wie bei Lautsprechern. Funktioniert daher nur, wenn man alleine im Raum ist. Dafür hört man auch die Geräusche von außerhalb, wenn der Ton nicht gerade super laut ist. Man kann also angesprochen werden, ohne den Kopfhörer abzunehmen. Bei einem geschlossenen Kopfhörer ist der Sound übertrieben gesagt wie in einer kleinen Box statt einem großen Raum, dafür ist der Ton nach außen hin abgeschirmt. Man hört nur sehr leise, was um einen herum passiert, die anderen hören aber auch nur sehr leise, was man über die Kopfhörer hört. Der perfekte Kopfhörer ist also je nach Situation ein anderer.

# Software

## Betriebssystem

{{% thumbnail "/images/NixOS-with-Pantheon-desktop-from-elementary-OS.png" alt="NixOS with Pantheon desktop from elementary OS" %}}{{% /thumbnail %}}

Ich verwende auf allen Computern [NixOS](https://nixos.org/) als Betriebssystem. Als Desktop-Umgebung verwende ich Pantheon von [elementary OS](https://elementary.io/). So habe ich ein zuverlässiges und benutzerfreundliches System.

Meine Konfiguration findest du auf [Codeberg](https://codeberg.org/davidak/nixos-config/).

## Programme

Als Browser verwende ich [Chromium](https://www.chromium.org/) mit [Einstellungen und Erweiterungen für bessere Privatsphäre](https://codeberg.org/davidak/nixos-config/src/branch/master/profiles/desktop.nix). Zu Testzwecken habe ich auch [Firefox](https://www.mozilla.org/de/firefox/) installiert.

Als Texteditor verwende ich [Atom](https://atom.io/). Das ist zwar ein [Electron](https://www.electronjs.org/)-Programm, aber dafür ähnlich benutzerfreundlich wie Sublime Text (was leider keine Freie Software ist). Auf der Kommandozeile verwende ich [micro](https://micro-editor.github.io/).

Als Passwort-Manager verwende ich [KeePassXC](https://keepassxc.org/).

Für die Synchronisierung von Dateien zwischen Geräten verwende ich [Syncthing](https://syncthing.net/).

Das Backup läuft mit [restic](https://restic.net/). Meine Konfiguration findest du auf [Codeberg](https://codeberg.org/davidak/nixos-config/src/branch/master/services/backup/default.nix).

Für Bildbearbeitung verwende ich [Gimp](https://www.gimp.org/).

Für die Entwicklung von RAW-Fotos verwende ich [darktable](https://www.darktable.org/).

Für einfache Audio-Bearbeitung verwende ich [Audacity](https://www.audacityteam.org/). Ich suche aber nach einer professionelleren Alternative.

Videos schneide ich mit [Kdenlive](https://kdenlive.org/). Ich bin aber sehr unzufrieden mit der Stabilität und dem Funktionsumfang. Ich möchte Alternativen wie [Olive](https://olivevideoeditor.org/), [Shotcut](https://www.shotcut.org/) und [Davinci Resolve](https://www.blackmagicdesign.com/products/davinciresolve/) ausprobieren.

Für Bildschirmaufzeichnung und Video-Streaming verwende ich [OBS Studio](https://obsproject.com/).

Die einzigen Programme auf meinem Computer, die keine [Freie Software](https://fsfe.org/freesoftware/freesoftware.de.html) sind, sind [Steam](https://store.steampowered.com/) und Spiele.

Wenn dich interessiert, welche Spiele ich spiele, schau dir mein [Profil auf Steam](https://steamcommunity.com/id/davidak) an.

# Vergangenheit

## Hardware

### iMac 11,3

Von 2010 bis 2018 war ein iMac mein Hauptcomputer. Er funktioniert zwar noch, mich stört aber, dass ich die Hardware nicht nachrüsten kann. Daher würde ich auch nicht wieder Apple Hardware kaufen.

- Intel Core [i7-870](https://ark.intel.com/content/www/us/en/ark/products/41315/intel-core-i7-870-processor-8m-cache-2-93-ghz.html) mit 2,93GHz
- 16 GB RAM
- ATI Radeon HD 5750 mit 1 GB VRAM
- 1 TB HDD
- 27" Bildschirm

### MacBook 2,1

Von 2007 bis 2017 habe ich ein MacBook verwendet. Bis ich den iMac gekauft habe war es mein Hauptcomputer. Es war mein erster selbst gekaufter Computer und erster Mac. Es funktioniert immer noch, ist aber langsam im Vergleich zu aktueller Hardware.

- Intel Core 2 Duo [T7200](https://ark.intel.com/content/www/us/en/ark/products/27255/intel-core-2-duo-processor-t7200-4m-cache-2-00-ghz-667-mhz-fsb.html) mit 2,0GHz
- 1 GB RAM (auf 2 GB aufgerüstet)
- 80 GB HDD (auf 120 GB SSD aufgerüstet)
- 13" Bildschirm

Vermutlich kam ich zum Mac, weil ich über iTunes, das ich bereits auf Windows benutzt habe, Podcasts wie [MacManiacs](https://macmaniacs.at/) gefunden habe.

### 4MBO Computer

Von 2002 bis 2008 verwendet. Es ist mein erster eigener Computer. Er funktioniert noch, ist aber sehr langsam und verbraucht verhältnismäßig viel Strom.

- AMD Athlon XP 1800+
- 256 MB RAM (auf 1 GB RAM aufgerüstet)
- 80 GB HDD (aufgerüstet)

## Software

### NixOS

2019 war der Pantheon Desktop von [elementary OS](https://elementary.io/) in [NixOS](https://nixos.org/) verfügbar, womit ich das perfekte System für mich hatte.

2017 bin ich auch mit meinem persönlichen Computer zu Linux gewechselt. Ich habe kurz davor [NixOS](https://nixos.org/) kennen gelernt und war sofort begeistert von der deklarativen Konfiguration. Das kannte ich schon von der Arbeit als Systemadministrator, bei der wir mit [Ansible](https://de.wikipedia.org/wiki/Ansible) etwa 250 Server administriert haben. Als Desktop hatte ich 2 Wochen [i3](https://de.wikipedia.org/wiki/I3_(Fenstermanager)) ausprobiert, was für mich aber nicht funktionierte. Dann zurück zu einem klassischen Desktop mit [Xfce](https://de.wikipedia.org/wiki/Xfce) und später [GNOME 3](https://de.wikipedia.org/wiki/Gnome).

## elementary OS

2015 hab ich auf meinem Arbeits-Notebook [elementary OS](https://elementary.io/) installiert. Vorher habe ich Mac OS X verwendet.

Seit 2008 wollte ich gerne [Linux](https://de.wikipedia.org/wiki/Linux) als Betriebssystem einsetzen, aber kein Desktop war so angenehm zu bedienen, wie ich das von [Mac OS X](https://de.wikipedia.org/wiki/MacOS) gewöhnt war. Mit dem Pantheon Desktop von [elementary OS](https://elementary.io/) gibt es eine Alternative, mit der ich zufrieden bin.

Ich hatte [elementary OS](https://elementary.io/) bereits einige Zeit auf meinem MacBook getestet.

## Mac OS X

2007 hab ich das MacBook gekauft. Es hatte [Mac OS X 10.4 (Tiger)](https://de.wikipedia.org/wiki/Mac_OS_X_Tiger) vorinstalliert. Ich war mit dem System super zufrieden. Es war intuitiv zu bedienen und sehr stabil. Jedes Update fühlte sich wie ein Forschritt an. So habe ich auch gerne 100€ dafür bezahlt.

Die beste Version ist meiner Meinung nach [Mac OS X 10.6 (Snow Leopard)](https://de.wikipedia.org/wiki/Mac_OS_X_Snow_Leopard). Nach dem Steve Jobs tot war wurde es schlechter, hatte [Sicherheitslücken](https://osxdaily.com/2017/11/28/macos-high-sierra-root-login-without-password-bug/), [Passwörter wurden in die Cloud geladen](https://de.wikipedia.org/wiki/Schl%C3%BCsselbund_(Software)#iCloud-Keychain) und das neue Design gefiel mir auch nicht mehr so gut.

Ich habe [Mac OS X](https://de.wikipedia.org/wiki/MacOS) etwa 10 Jahre verwendet.

## Ubuntu

Auf dem 4MBO Computer hab ich seit 2007 [Ubuntu](https://de.wikipedia.org/wiki/Ubuntu) benutzt. Eventuell auch schon früher.

2003 hat mir ein Freund meines Vaters [Knoppix](https://de.wikipedia.org/wiki/Knoppix) gezeigt. Das war das erste mal, dass ich von [Linux](https://de.wikipedia.org/wiki/Linux) gehört habe. Ich war begeistert, weil es viele Programme und Spiele installiert hatte und ich hab alle ausprobiert. Der [KDE Desktop](https://de.wikipedia.org/wiki/KDE) gefiel mir aber garnicht. Bis heute mag ich KDE nicht.

## Windows

Der 4MBO Computer kam mit [Windows XP](https://de.wikipedia.org/wiki/Microsoft_Windows_XP).

Zuvor hab ich an einem Computer meiner Eltern mit [Windows ME](https://de.wikipedia.org/wiki/Microsoft_Windows_Millennium_Edition) gearbeitet.

Ich habe etwa 6 Jahre lang [Windows XP](https://de.wikipedia.org/wiki/Microsoft_Windows_XP) benutzt. Später auch mal [Windows 7](https://de.wikipedia.org/wiki/Microsoft_Windows_7) und [Windows 10](https://de.wikipedia.org/wiki/Microsoft_Windows_10) für Spiele, die auf [Mac OS X](https://de.wikipedia.org/wiki/MacOS) oder [Linux](https://de.wikipedia.org/wiki/Linux) nicht liefen.

# Referenzen

Diese Seiten haben mich dazu inspiriert diese Seite zu erstellen:

- [Richard Stallman: How I do my computing](https://stallman.org/stallman-computing.html)
- [uses this: site with more than 1000 interviews](https://usesthis.com/)
- [A list of /uses pages](https://uses.tech/)
