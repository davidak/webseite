.. title: Projects I follow
.. slug: projects-i-follow
.. date: 2020-10-04 19:05:00 UTC+02:00
.. tags: project
.. category:
.. link:
.. description:
.. type: text

You can find projects I follow on this page including what I'm actually waiting for. Most of the time, they are	promising projects I want to use, but they are missing features I need.

This page is in english, because that's the main language in most of these projects.


## [Snowdrift.coop](https://snowdrift.coop/)

They develop an innovative mechanism to fund public goods and implement it in their own platform. Ultimately, I want to use such a platform to fund projects I use.

### Milestones

- [ ] Have the new crowdmatching mechanism ready and published, so I can talk about and link to it
- [ ] Have support for SEPA payments ([#156](https://gitlab.com/snowdrift/snowdrift/-/issues/156))
- [ ] Have their own funding ready using the new mechanism, so i can announce their solution and suggest to fund them
- [ ] Have a first external project to fund, so i can test it and see how well it works
- [ ] Be able to fund my favorite projects there. when their funding approach is successful, projects will join

I got involved to get there faster.


## [WTF eG](https://vebit.xyz/)

They are founding a workers coop to enable new ways of working in IT. I'm a founding member.

### Milestones

- [ ] Actually found the coop (planned for 2021)
- [ ] Be able to write invoices, so I can do freelance business without own legal entity


## [Mobile NixOS](https://mobile.nixos.org/)

A project from Samuel Dionne-Riel. He ports NixOS to mobile devices that usually run Android. Of course, i would love to run NixOS also on my smartphone!

### Milestones

- [ ] Run Mobile NixOS on a device i own (i consider even buying a new one for this)
- [ ] Be able to use it for daily tasks


## [ForgeFed](https://forgefed.peers.community/)

A federation protocol for version control services. This will help to make decentralized platforms like Codeberg more attractive to users of popular, proprietary platforms like GitHub und GitLab.

### Milestones

- [ ] I want to be able to create issues and merge requests from my account on a Gitea instance to a GitLab instance
- [ ] I want to be able to favorite projects on other instances


## [GNU Taler](https://taler.net/en/index.html)

They create an open source online payment system that protects the payers privacy and prevents fraud at the same time.

### Milestones

- [ ] Release a stable version 1.0 (see [roadmap](https://bugs.gnunet.org/roadmap_page.php?project_id=23))
- [ ] Be able to transfer Euros to my wallet
- [ ] Be usable with my bank (they actually evaluate it internally)
- [ ] Be able to pay something online

This will be useful to fund public goods like free software projects.


## [apertus°](https://www.apertus.org/)

They are creating a digital cinema camera as open hardware, called the [AXIOM Beta](https://www.apertus.org/axiom-beta). I contributed in their crowdfunding to get one.

### Milestones

- [ ] Power Board V2
- [ ] Full Enclosure

See the [Development Status](https://www.apertus.org/axiom-beta-status) for the latest updates.


(I know that the Markdown checkboxes don't render properly)
