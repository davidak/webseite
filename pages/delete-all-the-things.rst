.. title: Delete all the things
.. slug: delete-all-the-things
.. date: 2016-03-03 04:54:50 UTC+01:00
.. tags:
.. description:
.. type: text

Ich liebe es nach dem Aufräumen des Rechners viele Dateien aus dem Papierkorb zu löschen.

.. image:: /images/delete-all-the-things.jpg

Mac OS X
--------

.. image:: /images/mac-os-x-papierkorb-leeren.png
	:alt: Papierkorb mit 2549784 Dateien leeren auf Mac OS X 10.10.5 Yosemite

NixOS
-----

Alte Pakete und Konfigurationen löschen::

	[root@nixos:~]# nix-env --delete-generations old
	removing generation 1
	removing generation 2
	removing generation 3
	...

	[root@nixos:~]# nix-collect-garbage -d
	...
	deleting '/nix/store/trash'
	deleting unused links...
	note: currently hard linking saves 623.78 MiB
	277199 store paths deleted, 148826.27 MiB freed

Nix-Store optimieren, in dem identische Dateien durch Hardlinks ersetzt werden::

	[root@nixos:~]# nix optimise-store
	[38732 paths optimised, 9941.6 MiB / 551922 inodes freed]

systemd journal
---------------

Alle Einträge außer die der letzten 2 Tage aus dem Journal löschen::

	[root@nixos:~]# journalctl --vacuum-time=2d
	...
	Vacuuming done, freed 3.8G of archived journals from /var/log/journal/7adea793c51646eb9781f0a4f62322d6.
