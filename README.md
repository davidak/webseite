# davidak.de

Dies ist der Quelltext meiner persönlichen Webseite.

Die [Version 12](https://codeberg.org/davidak/webseite/milestone/314) befindet sich noch in Entwicklung, ist aber schon online.

Wie die vorherigen Versionen aussahen kannst du auf auf der Seite [Rückblick Webseite](https://davidak.de/rueckblick-webseite/) sehen.

Bei Fragen gerne bei mir melden. Kontaktmöglichkeiten findest du auf der Webseite.

## Features

- Blog (Artikel)
- Seiten
- Profiltext
- Projekt Übersicht
- Kommentare (über [Isso](https://isso-comments.de/))

## Ziele

- Wissen und Erfahrung teilen (Seiten, Artikel)
- Austausch (Kommentare)
- Zeigen, woran ich arbeite (Projekte)
- Mich vorstellen (Profil)
- Kontaktmöglichkeiten (E-Mail, Kontaktformular)
- Links zu anderen Online-Präsenzen

### Sekundärziele

- Wissen und Kultur erhalten durch langfristiges Betreiben (Viele Blogs von 2006 sind nicht mehr online)
- Unabhängigkeit von profitorientierten Plattformen
- Spaß am Lernen neuer Technik (Wordpress, Static Site Generator, Webdesign)

## Technik

Es wird eine statische HTML-Seite mit [Nikola](https://getnikola.com/) aus Markdown-Dateien generiert.

Auch die Konfiguration des Webservers ([Caddy](https://caddyserver.com/)) ist in Form eines [Caddyfile](files/Caddyfile) enthalten.

### Deployment

Führe `nix-shell` aus, um eine temporäre Shell mit allen benötigten Abhängigkeiten zu erhalten.
Dafür muss nur der [Nix Paketmanager](https://nixos.org/nix/) installiert sein.

Mit diesen beiden Befehlen wird die Webseite generiert und auf den Server hochgeladen:

    nikola build && nikola deploy

### Publish to [IPFS](https://ipfs.io/)

    ipfs add -r output/
    ipfs name publish SITE_HASH

## Lizenz

Inhalte wie Texte und Bilder sind unter [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/) lizensiert und Code unter GPL-3.0-or-later, wenn nicht anders angegeben.

Alle Dateien mit der Endung `.nix` sind unter MIT lizensiert, um mit diesem Ökosystem kompatibel zu sein.
