.. title: Wie sieht ein Dateisystem tatsächlich aus?
.. slug: wie-sieht-ein-dateisystem-tatsaechlich-aus
.. date: 2024-05-21 18:00:00 UTC+02:00
.. tags: Filesystem
.. category:
.. link:
.. description:
.. type: text

Ich habe verschiedene Dateisysteme visualisiert.
<!-- TEASER_END -->

Dafür habe ich auf einer zweiten internen Festplatte (SSD) eine neue Partition mit dem entsprechenden Dateisystem erstellt, einige Beispiel-Dateien darauf kopiert und dann die Daten der Partition mit [ffmpeg](https://ffmpeg.org/) als RGB 24-bit interpretiert und als Bilddatei gespeichert. Ich habe auch [andere Formate](https://ffmpeg.org/ffmpeg-all.html#Advanced-Video-options) getestet, aber dieses sah am besten aus.

```
sudo ffmpeg -f rawvideo -pixel_format rgb24 -s 2560x1440 -i /dev/nvme0n1p2 -vframes 1 filesystem-picture-ext4.png
```

Auch die Bezeichnungen habe ich mit einem Kommandozeilen-Tool hinzugefügt, nämlich mit [imagemagic](https://imagemagick.org/).

```
convert '/home/davidak/tmp/scrapyard/filesystem-picture-ext3.png' \
-gravity South \
-font "/nix/store/0s0lnjh2yy08lwglb5zhwnp9aaad0pic-open-sans-1.11/share/fonts/truetype/OpenSans-ExtraBold.ttf" \
-pointsize 100 \
-fill white \
-box black \
-annotate +0+60 " Third Extended Filesystem (ext3) " \
output_image.png
```

Die Idee zu diesem Projekt hatte ich schon 2021 durch einen [Post von Jamie](https://twitter.com/JJJollyjim/status/1469605662032154627), in dem sie ein ähnliches Ergebnis hatte, in dem sie die Daten der Partition an `/dev/fb0` weitergeleitet hat.

Du kannst die Bilder auch in einem neuen Tab/Fenster öffnen, um sie in voller Auflösung angezeigt zu bekommen.

## FAT32

{{% thumbnail "/images/filesystem-picture-fat32.png" alt="Visualisierung File Allocation Table (FAT32) Dateisystem" %}}{{% /thumbnail %}}

## NTFS

{{% thumbnail "/images/filesystem-picture-ntfs.png" alt="Visualisierung New Technology File System (NTFS)" %}}{{% /thumbnail %}}

## ext2

{{% thumbnail "/images/filesystem-picture-ext2.png" alt="Visualisierung Second Extended Filesystem (ext2)" %}}{{% /thumbnail %}}

## ext3

{{% thumbnail "/images/filesystem-picture-ext3.png" alt="Visualisierung Third Extended Filesystem (ext3)" %}}{{% /thumbnail %}}

## ext4

{{% thumbnail "/images/filesystem-picture-ext4.png" alt="Visualisierung Fourth Extended Filesystem (ext4)" %}}{{% /thumbnail %}}

## XFS

{{% thumbnail "/images/filesystem-picture-xfs.png" alt="Visualisierung X File System (XFS)" %}}{{% /thumbnail %}}

## ZFS

{{% thumbnail "/images/filesystem-picture-zfs.png" alt="Visualisierung Zettabyte File System (ZFS)" %}}{{% /thumbnail %}}

## Btrfs

{{% thumbnail "/images/filesystem-picture-btrfs.png" alt="Visualisierung B-tree File System (Btrfs)" %}}{{% /thumbnail %}}

## Bcachefs

{{% thumbnail "/images/filesystem-picture-bcachefs.png" alt="Visualisierung Bcachefs" %}}{{% /thumbnail %}}

## ISO 9660

{{% thumbnail "/images/filesystem-picture-ISO-9660.png" alt="Visualisierung ISO 9660 (CD) Dateisystem" %}}{{% /thumbnail %}}

Um das Dateisystem von CDs zu visualisieren habe ich das Image `ubuntu-8.04-desktop-i386.iso` genommen.

Leider habe ich gerade keinen funktionierenden Mac zur Verfügung, um es auch mit dessen Dateisystemen (HFS, HFS+ und APFS) zu machen.

Die Bilder stehen wie immer unter der [Creative Commons BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.en) Lizenz und dürfen entsprechend verwendet werden. Ich habe schon Ideen.
