<!--
.. title: Was ist Identität?
.. slug: was-ist-identitaet
.. date: 2021-12-29 13:02:02 UTC+01:00
.. tags: Identität, Selbstfindung
.. category:
.. link:
.. description:
.. type: text
-->

Identität ist die Antwort auf die Frage "**Wer bin ich?**".
<!-- TEASER_END -->

Diese Frage stellt sich vor allem in einer **Sinnkrise** oder wenn man sich **unsicher fühlt**.

Identität ist die Gesamtheit der **Eigentümlichkeiten**, die eine Person **kennzeichnen** und als **Individuum** von anderen **unterscheiden**. In unterschiedlichen **Rollen** und **Situationen** zeigt man nur einen Teil seiner Identität. Man bleibt dennoch der gleiche Mensch.

Die Identität entwickelt sich durch **Resonanz** mit anderen Menschen. Dies geschieht in einem Wechselspiel von „**Dazugehören**“ und „**Abgrenzen**“. **Zugehörigkeit** zu einer Gruppe, z.B. politische Bewegung, Fan einer Sportmannschaft oder Mitglied in einem Verein. Aber auch in **Abgrenzung** zu anderen Menschen und Gruppen, z.B. zu den Eltern, als Nichtraucher oder Antifa. Man bekommt auch Inspiration, wie man sein könnte und entwickelt so ein **Wunschbild** von sich selbst.

Die Identität unterliegt einer **kontinuiertlichen Entwicklung**. Sie passt sich **flexibel** an die aktuelle Lebenssituation an.

Nach diesem etablierten Modell ist die Identität die **Schnittmenge** von **Selbstbild**, **Fremdbild** und **Wunschbild**.

![Identitätsmodell](/images/Identität-Modell.png)

(Copyright 2021 davidak. Freie Lizenz: [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). Als [PNG](/images/Identität-Modell.png), [SVG](/images/Identität-Modell.svg) oder [LibreOffice Impress](https://de.libreoffice.org/discover/impress/) [Quelldatei](/images/Identität-Modell.odp) runterladen)

Wenn sich **Wunschbild** und **Fremdbild** überschneiden ist das der Bereich, bei dem Andere schon mehr in einem sehen als man selbst.

Wenn sich **Fremdbild** und **Selbstbild** überschneiden ist das der Bereich, auf den man nicht besonders stolz ist. Man entspricht dort noch nicht den eigenen Idealen.

Wenn sich **Selbstbild** und **Wunschbild** überschneiden ist das der Bereich, bei dem man sich entweder selbst überschätzt oder die Anderen das Wachstum noch nicht sehen.

Aus dem Modell ergibt sich, dass wir nicht die volle Kontrolle über unsere Identität haben. So fühlt man sich z.B. unsicher in einer neuen Gruppe, da sie wenig von einem wissen, also das Fremdbild klein ist und somit auch die Identität, obwohl sich am Selbstbild und Wunschbild nichts geändert hat.

Für die Identität ist es wichtig das Selbstbild eindeutig nach außen zu kommuniziert. Denn wenn man merkt, dass andere einen so wahrnehmen wie man sich selbst, ist das eine Bestätigung und stärkt das **Selbstvertrauen**. Wenn Andere ein ganz anderes Bild von einem haben als man selbst beeinträchtigt das das Selbstvertrauen. Es kann zu Missverständnissen kommen. Wenn nicht klar ist, wie eine andere Person einen sieht, ist es für einen besser vom positiven auszugehen.

Bei **toxischen Beziehungen** und **Mobbing** wird einem ein gefälschtes Fremdbild präsentiert, um einem zu schaden oder einen zu manipulieren. Solche Beziehungen sollte man zum Schutz der eigenen Identität beenden.

Häufig entstehen **Krisen**, wenn sich die Kreise gegeneinander verschoben haben und die Schnittmenge kleiner wird. Sie sind aber auch immer eine Gelegenheit zu wachsen, so dass die Identität nachher größer ist als vorher.

## Quellen

- [Identität – Wiktionary](https://de.wiktionary.org/wiki/Identit%C3%A4t)
- [Identität – Wikipedia](https://de.wikipedia.org/wiki/Identit%C3%A4t)
- [Kultur Konfetti - Was ist Identität?](https://www.youtube.com/watch?v=oWEHJRXF8J0)
- [Identität heißt auch: Zugehörigkeit zu anderen](https://www.youtube.com/watch?v=mgshrD1K1R8)
- [IDENTITÄT | Die große Frage nach dem eigenen Ich](https://www.youtube.com/watch?v=AH5gtVpXCNI)
- [Wer bin ich wirklich? So findest du es heraus](https://www.youtube.com/watch?v=lgIv0WKy_IQ)
- [Multikollektivität und Identität](https://www.youtube.com/watch?v=SG3LVPVur7Y)
- [Was ist Identität? - Ben Teggemann](https://www.youtube.com/watch?v=3iZDTBcbfuU)
- ["Wer bin ich? Frag doch die anderen!"](https://www.ardmediathek.de/video/tele-akademie/wer-bin-ich-frag-doch-die-anderen-eva-jaeggi/swr-fernsehen/Y3JpZDovL3N3ci5kZS9hZXgvbzEzODk3OTc/) Vortrag von Eva Jaeggi (45 Min.)
- [Die Kraft des positiven Denkens](https://www.julaonline.de/metabild/) (Blogartikel einer Trans-Frau)
