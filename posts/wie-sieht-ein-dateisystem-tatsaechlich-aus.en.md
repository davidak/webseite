.. title: What does a filesystem really look like?
.. slug: what-does-a-filesystem-really-look-like
.. date: 2024-05-21 18:00:00 UTC+02:00
.. tags: Filesystem
.. category:
.. link:
.. description:
.. type: text

I have visualized different file systems.
<!-- TEASER_END -->

I created a new partition on a second internal hard disk (SSD) with the respective file system, copied some example files to it and then interpreted the data of the partition with [ffmpeg](https://ffmpeg.org/) as RGB 24-bit and saved it as an picture file. I also tested [other formats](https://ffmpeg.org/ffmpeg-all.html#Advanced-Video-options), but this one looked the best.

```
sudo ffmpeg -f rawvideo -pixel_format rgb24 -s 2560x1440 -i /dev/nvme0n1p2 -vframes 1 filesystem-picture-ext4.png
```

I also added the labels using a command line tool, namely [imagemagic](https://imagemagick.org/).

```
convert '/home/davidak/tmp/scrapyard/filesystem-picture-ext3.png' \
-gravity South \
-font "/nix/store/0s0lnjh2yy08lwglb5zhwnp9aaad0pic-open-sans-1.11/share/fonts/truetype/OpenSans-ExtraBold.ttf" \
-pointsize 100 \
-fill white \
-box black \
-annotate +0+60 " Third Extended Filesystem (ext3) " \
output_image.png
```

I got the idea for this project back in 2021 from a [post by Jamie](https://twitter.com/JJJollyjim/status/1469605662032154627) where she had a similar result by forwarding the partition data to `/dev/fb0`.

You can open the images in a new tab/window to see them full resolution.

## FAT32

{{% thumbnail "/images/filesystem-picture-fat32.png" alt="Visualization of File Allocation Table (FAT32) file system" %}}{{% /thumbnail %}}

## NTFS

{{% thumbnail "/images/filesystem-picture-ntfs.png" alt="Visualization of New Technology File System (NTFS)" %}}{{% /thumbnail %}}

## ext2

{{% thumbnail "/images/filesystem-picture-ext2.png" alt="Visualization of Second Extended Filesystem (ext2)" %}}{{% /thumbnail %}}

## ext3

{{% thumbnail "/images/filesystem-picture-ext3.png" alt="Visualization of Third Extended Filesystem (ext3)" %}}{{% /thumbnail %}}

## ext4

{{% thumbnail "/images/filesystem-picture-ext4.png" alt="Visualization of Fourth Extended Filesystem (ext4)" %}}{{% /thumbnail %}}

## XFS

{{% thumbnail "/images/filesystem-picture-xfs.png" alt="Visualization of X File System (XFS)" %}}{{% /thumbnail %}}

## ZFS

{{% thumbnail "/images/filesystem-picture-zfs.png" alt="Visualization of Zettabyte File System (ZFS)" %}}{{% /thumbnail %}}

## Btrfs

{{% thumbnail "/images/filesystem-picture-btrfs.png" alt="Visualization of B-tree File System (Btrfs)" %}}{{% /thumbnail %}}

## Bcachefs

{{% thumbnail "/images/filesystem-picture-bcachefs.png" alt="Visualization of Bcachefs" %}}{{% /thumbnail %}}

## ISO 9660

{{% thumbnail "/images/filesystem-picture-ISO-9660.png" alt="Visualization of ISO 9660 (CD) file system" %}}{{% /thumbnail %}}

To visualize the file system of CDs I used the image `ubuntu-8.04-desktop-i386.iso`.

Unfortunately I don't have a working Mac at hand to do it with its file systems (HFS, HFS+ and APFS).

As always, the images are licensed under the [Creative Commons BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license and can be used accordingly. I already have ideas.
