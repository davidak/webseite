{#  -*- coding: utf-8 -*- #}
{% import 'post_helper.tmpl' as helper with context %}
{% import 'post_header.tmpl' as pheader with context %}
{% extends 'base.tmpl' %}

{% block extra_head %}
    {{ super() }}
    {% if post.meta('keywords') %}
    <meta name="keywords" content="{{ post.meta('keywords')|e }}">
    {% endif %}
    <meta name="author" content="{{ post.author()|e }}">
    {{ helper.open_graph_metadata(post) }}
    {{ helper.twitter_card_information(post) }}
    {{ helper.meta_translations(post) }}
    <link href="css/fotorama.css" rel="stylesheet">
    <style>
      @media (min-width: 768px) {
          .pull-sm-left {
              float: left !important;
          }
          .pull-sm-right {
              float: right !important;
          }
          .pull-sm-none {
              float: none !important;
          }
      }
      @media (min-width: 992px) {
          .profiltext {
            font-size: 112%;
          }
          .lead {
            font-size: 180%;
          }
          .profilepicture {
            width: 50%;
            margin-left: 35px;
            margin-bottom: 35px;
          }
          .pull-md-left {
              float: left !important;
          }
          .pull-md-right {
              float: right !important;
          }
          .pull-md-none {
              float: none !important;
          }
      }
      @media (min-width: 1200px) {
          .pull-lg-left {
              float: left !important;
          }
          .pull-lg-right {
              float: right !important;
          }
          .pull-lg-none {
              float: none !important;
          }
      }
      .pull-none {
          float: none !important;
      }
    </style>
{% endblock %}

{% block content %}
    <div class="e-content entry-content" itemprop="text">

      <div class="pull-md-right profilepicture">
        <div class="fotorama" data-fit="cover" data-transition="crossfade" data-loop="true" data-shuffle="true" data-keyboard="true">
          <img src="davidak-Foto-1.jpg" alt="Foto von davidak">
          <img src="davidak-Foto-2.jpg" alt="Foto von davidak">
          <img src="davidak-Foto-3.jpg" alt="Foto von davidak">
          <img src="davidak-Foto-4.jpg" alt="Foto von davidak">
          <img src="davidak-Foto-5.jpg" alt="Foto von davidak">
        </div>
        <br>
        <p class="text-center">
          Software-Entwickler, Content Creator, Minimalist, Gamer, Aktivist<br><br>
          <b>E-Mail:</b> post at davidak.de<br>
          <b>Öffentlicher PGP-Schlüssel:</b> <a href="pgp-publickey-davidak.asc">B60197B0</a><br>
          <!--oder <a href="#">Kontaktformular</a> gibt es noch nicht :\ -->
          <br>
          <b>Software Repositories:</b> <a href="https://codeberg.org/davidak">Codeberg</a><br>
          <b>Social Media:</b> <a rel="me" href="https://chaos.social/@davidak">Fediverse</a><br>
          <br>
          <b>Sprachen:</b> deutsch, englisch<br>
          <b>Wohnort:</b> <a href="https://de.wikipedia.org/wiki/Osnabr%C3%BCck">Osnabrück</a><br>
          <b>Zeitzone: </b><a href="https://www.timeanddate.com/time/zones/cest">CEST</a> (UTC +2) im Sommer<br>
          und <a href="https://www.timeanddate.com/time/zones/cet">CET</a> (UTC +1) im Winter<br>
        </p>
      </div>

      <div class="profiltext">
        {{ post.text() }}
      </div>

      <h3>Meine Mission</h3>

      <p>Darauf möchte ich mich die nächsten Jahre fokussieren.</p>

      <div class="card-deck">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Freie Software zugänglicher machen</h5>
            <p class="card-text">Das bedeutet im Detail: Die Vorteile von Freier Software verständlich und überzeugend erklären, die Qualität, Bedienbarkeit und Auffindbarkeit verbessern sowie die Entwicklung nachhaltig finanzieren. Ich möchte generelle Empfehlungen für Benutzer und Entwickler erarbeiten und auch praktisch an Projekten mitarbeiten.</p>
            <p class="card-text">Das Ziel ist erreicht, wenn für jede Aufgabe Freie Software die erste Wahl ist. Beispiele, wo das bereits der Fall ist, sind <a href="https://obsproject.com/">Open Broadcaster Software (OBS)</a> bei Streamern und <a href="https://krita.org/">Krita</a> bei Künstlern, die digital zeichnen.</p>
          </div>
        </div>
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Eine Alternative zum Kapitalismus finden</h5>
            <p class="card-text">Durch den Kapitalismus werden Menschen, Tiere und die Umwelt ausgebeutet. Wenn wir unseren Lebensstil nicht radikal ändern, wird es für uns in wenigen Jahren nicht mehr möglich sein auf der Erde zu leben. Ich bin davon überzeugt, dass es die Lösung schon gibt. Wir müssen sie nur finden und umsetzen.</p>
            <p class="card-text">Das Ziel ist erreicht, wenn ich eine Möglichkeit gefunden habe, mich nicht mehr am Kapitalismus zu beteiligen. Ich möchte Menschen inspirieren und zeigen, dass so ein gutes Leben möglich ist.</p>
          </div>
        </div>
      </div>

      <br>

      <p>Ich kann die Welt nicht alleine retten. Daher freue ich mich immer, wenn ich Menschen finde, die an den gleichen Zielen arbeiten.</p>

      <br>

      <h3>Meine Projekte</h3>

      <div class="card-deck">
        <div class="card">
          <a href="https://www.youtube.com/watch?v=9D1lMTwcLgY"><img src="Wortspiele-a-la-Polysemia-Fanvideo.jpg" class="card-img-top" alt="Wortspiele a la Polysemia Fanvideo"></a>
          <div class="card-body">
            <h5 class="card-title">Wortspiele à la Polysemia</h5>
            <p class="card-text">Ein Fanvideo zur Sendung <a href="https://www.youtube.com/playlist?list=PL4F720557B5A9902E">Polysemia</a>.</p>
          </div>
        </div>
        <div class="card">
          <a href="http://aquaregia.de/"><img src="AquaRegia-Band-Webseite.jpg" class="card-img-top" alt="AquaRegia Band Webseite"></a>
          <div class="card-body">
            <h5 class="card-title">AquaRegia</h5>
            <p class="card-text">Webseite und Fotos für die Band <a href="http://aquaregia.de/">AquaRegia</a>.</p>
          </div>
        </div>
        <div class="card">
          <a href="http://brennblatt.de/"><img src="Karl-Brennblatt-Webseite.jpg" class="card-img-top" alt="Karl Brennblatt Webseite"></a>
          <div class="card-body">
            <h5 class="card-title">Karl Brennblatt</h5>
            <p class="card-text">Webseite für den Künstler <a href="http://brennblatt.de/">Karl Brennblatt</a>.</p>
          </div>
        </div>
      </div>

      <br>

      <div class="card-deck">
        <div class="card">
          <a href="https://davidak.de/gna/"><img src="GNA-Clan-Webseite.jpg" class="card-img-top" alt="GNA Clan Webseite"></a>
          <div class="card-body">
            <h5 class="card-title">GNA Clan</h5>
            <p class="card-text">Webseite und Game-Server für den <a href="https://davidak.de/gna/">GNA Clan</a>.</p>
          </div>
        </div>
        <div class="card">
          <a href="https://davidak.de/random-graph/"><img src="Random-Graph.jpg" class="card-img-top" alt="Random Graph"></a>
          <div class="card-body">
            <h5 class="card-title">Random Graph</h5>
            <p class="card-text">Echtzeit Graph mit zufälligen Daten.</p>
          </div>
        </div>
        <div class="card">
          <a href="https://satzgenerator.de/"><img src="Satzgenerator.jpg" class="card-img-top" alt="Satzgenerator"></a>
          <div class="card-body">
            <h5 class="card-title">Satzgenerator</h5>
            <p class="card-text">Zufällige Sätze generieren, bewerten und teilen.</p>
          </div>
        </div>
      </div>

      <br>

      <table class="table table-striped">
        <tr>
          <th><a href="https://media.ccc.de/b/congress">Chaos Communication Congress/Camp</a></th>
          <td>Kameramann und Regie/Bildmischer bei vielen Vorträgen. Live gestreamt ins Internetz.</td>
        </tr>
        <tr>
          <th><a href="http://concert.arte.tv/de/collections/moers-festival-2014">Moers Festival 2014</a></th>
          <td>Kameramann bei allen Bands außer einer, bei der ich <a href="https://youtu.be/ZTOrPhqwkA8">Regie/Bildmischer</a> war. Live bei ARTE.</td>
        </tr>
        <tr>
          <th><a href="https://github.com/davidak/random-vcard-generator">Random VCard-Generator</a></th>
          <td>Das Kommandozeilen-Programm generiert VCards mit zufälligen, aber plausiblen Daten.</td>
        </tr>
        <tr>
          <th><a href="https://pyzufall.readthedocs.org/de/latest/">PyZufall</a></th>
          <td>Die Python-Bibliothek PyZufall beinhaltet diverse Funktionen für das Generieren zufälliger Daten.</td>
        </tr>
        <tr>
          <th><a href="https://davidak.de/personen/">Personendatenbank</a></th>
          <td>Daten von über 10.000 Personen. Zufällig generiert mit einem <a href="https://davidak.de/wiki/perl/personendatengenerator">Perl-Script</a>.</td>
        </tr>
        <tr>
          <th><a href="https://davidak.de/wiki/perl/namengenerator">Namengenerator</a></th>
          <td>Ein Perl-Script, um Namen zu generieren.</td>
        </tr>

        <tr>
          <th><a href="https://github.com/davidak/python-peniscoin-miner">Peniscoin Miner</a></th>
          <td>Eine Parodie auf <a href="https://de.wikipedia.org/wiki/Bitcoin">Bitcoin</a> und andere <a href="https://de.wikipedia.org/wiki/Kryptow%C3%A4hrung">Kryptowährungen</a> in Form eines lauffähigen Programms.</td>
        </tr>
      </table>

    </div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- disable tracking -->
<!-- waiting for new release of fotorama without visitor tracking https://github.com/artpolikarpov/fotorama/commit/fb4145f814fa1b4b25471b0c121d151e306237e4 -->
<script>
  blockFotoramaData = true;
</script>
<script src="js/fotorama.js"></script>
{% endblock %}

{% block sourcelink %}
{% if show_sourcelink %}
    <li>
    <a href="{{ post.source_link() }}" id="sourcelink">{{ messages("Source") }}</a>
    </li>
{% endif %}
{% endblock %}
